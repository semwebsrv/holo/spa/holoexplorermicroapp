# HoloExplorerMicroApp

HoloExplorer is a generic microapp for the holo framework that allows a permissioned user to explore the
resources exposed by all other holo microapps via a generic explorer interface which allows the user to
invoke the CRUD options exposed for that resource without interacting with a bespoke application interface

